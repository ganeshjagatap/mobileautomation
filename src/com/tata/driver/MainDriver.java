package com.tata.driver;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.testng.TestNG;

import com.tata.libraries.Resultsummary;
import com.tata.libraries.SendMailForMultipleReceipient;
import com.tata.libraries.SetUp;
import com.tata.libraries.ZipResults;

//*************************************************************************************************************************
//Generic Class name :MainDriver
//Description :Framework starter engine which triggers framework and ends
//Author: Ganesh Jagatap
//Last Modified Date: 17-08-2016
//*************************************************************************************************************************	
public class MainDriver {
	

	public static int pass = 0;
	
	public static int fail = 0;
	public static int rowcount;
	public static String Exectime;
	public static long Extime;
	public static long endTime;
	public static long startTime;
	
	public static String Exectime1;
	public static long Extime1;
	public static long endTime1;
	public static long startTime1;
	public static void main(String[] args) throws Exception {
		String s2="";
	
		//for(int j=0;j<=2;j++){
		System.out.println("Framework Starts");
		
		
		/*CommandLine launchEmul = new CommandLine("cmd");
		DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
		DefaultExecutor executor = new DefaultExecutor();
		launchEmul.addArgument("/c");
		launchEmul.addArgument("C:\\Program Files\\Genymobile\\Genymotion//player");
		
	//	launchEmul.addArgument("–address", false);
		//launchEmul.addArgument("127.0.0.1");
		launchEmul.addArgument("-–vm-name");
		launchEmul.addArgument("SamsungGalaxyS4");
		

		executor.setExitValue(1);

		executor.execute(launchEmul, resultHandler);*/
		
		
		
		
		/*CommandLine command = new CommandLine("cmd");
		command.addArgument("/c");
		command.addArgument("D://Appium//Appium//node.exe");
		command.addArgument("D://Appium//Appium//node_modules//appium//bin//appium.js");
		command.addArgument("--address");
		command.addArgument("127.0.0.1");
		command.addArgument("--bootstrap-port");
		command.addArgument("4724");
		command.addArgument("--no-reset");
	//	command.addArgument("--log");
		//command.addArgument("C://Users//...//log//appiumLogs.txt");
		DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
		DefaultExecutor executor = new DefaultExecutor();
		executor.setExitValue(1);
		executor.execute(command, resultHandler);*/
		MainDriver.startTime1 = System.currentTimeMillis();
		SetUp br = new SetUp();
		String runScripts = br.testConfigFile.getProperty("runScripts");
		Resultsummary rs = new Resultsummary();
		rs.createSummaryFile();
		Scripts s = new Scripts();
		ArrayList<String> scripexec = s.scr();
		TestNG testNGInstance = new TestNG();
		// using java reflections
		
		int n = scripexec.size();
		System.out.println("exec count " + n);
		@SuppressWarnings("rawtypes")
		Class[] testSuite = new Class[scripexec.size()];
		for (int i = 0; i < scripexec.size(); i++) {
		
			testSuite[i] = Class.forName("com.tata." + runScripts + "."
					+ scripexec.get(i));
		

		}
		testNGInstance.setTestClasses(testSuite);
		testNGInstance.run();
		ZipResults z = new ZipResults();
		z.zipRes();
		
		
		 

		SendMailForMultipleReceipient send = new SendMailForMultipleReceipient();
		send.sendMail("ganeshj", SendMailForMultipleReceipient.Esplit,
				"TATA Mobile Automation Execution Report");
		System.out.println("framework end");
		System.out.println("start commant");
		/*command.addArgument("/c");
		command.addArgument("taskkill");
		command.addArgument("/F");
		command.addArgument("/IM");
		command.addArgument("node.exe");
		System.out.println("stop");*/
		MainDriver.endTime1 = System.currentTimeMillis();
		MainDriver.Extime1 = MainDriver.endTime1-MainDriver.startTime1;
					System.out.println(MainDriver.Extime1);
					MainDriver.Exectime = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(MainDriver.Extime1),
				    TimeUnit.MILLISECONDS.toMinutes(MainDriver.Extime) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(MainDriver.Extime1)),
				    TimeUnit.MILLISECONDS.toSeconds(MainDriver.Extime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(MainDriver.Extime1)));
					System.out.println("Total elapsed time to run all scripts :"+ (MainDriver.Exectime1));
	
}}