package com.tata.driver;

import java.util.ArrayList;

import com.tata.libraries.ExcelLibrary;

//*************************************************************************************************************************
//Generic Class name :Scripts
//Description :Controlling and executing scripts as per excel data from excel sheet
//Author: Ganesh Jagatap
//Last Modified Date: 17-08-2016
//*************************************************************************************************************************	

public class Scripts {

	ExcelLibrary lib = new ExcelLibrary();

	@SuppressWarnings("unchecked")
	public ArrayList<String> scr() {
		@SuppressWarnings("rawtypes")
		ArrayList ar = new ArrayList();
		int step1 = lib.getRowCount("Scenarios");

		for (int i = 1; i <= step1; i++) {
			String s1 = lib.getExcelData("Scenarios", i, 2);
			if (s1.equalsIgnoreCase("yes")) {
				String s2 = lib.getExcelData("Scenarios", i, 1);
				ar.add(s2);
			}
			System.out.println("scenarios " + s1);

		}

		return ar;
	}

}