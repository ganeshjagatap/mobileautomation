package com.tata.scripts;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

import java.io.File;
import java.io.FileInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;



public class test2 {
	 public static AppiumDriver driver;
		private FileInputStream configFile = null;
		public Properties testConfigFile = null;
		String chromedrvpath = null;
		String iedrvpath = null;

	@Test
	public void run1() throws MalformedURLException, InterruptedException{
		try {
			configFile = new FileInputStream("./config.properties");
			testConfigFile = new Properties();
			testConfigFile.load(configFile);
		} catch (Exception e) {

			e.printStackTrace();
		}
		String ScrRunInterface = testConfigFile.getProperty("ScrRunInterface");
		String apk = testConfigFile.getProperty("apk");
		System.out.println("apk pathhh "+apk);
		File app = new File(apk);
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(CapabilityType.BROWSER_NAME, "");
		 capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "4.4");
		 capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME,"appium"); 
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "192.168.56.101:5555"); 
		capabilities.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
		
       capabilities.setCapability("app-package", "com.tatadhp.consumer.staging");  
       capabilities.setCapability("app-activity", "com.tatadhp.consumer.ui.activities.WalkThroughActivity");  
       capabilities.setCapability("app-wait-activity","com.tatadhp.consumer.ui.activities.WalkThroughActivity");
		capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 80);
		System.out.println("Start1111");
      driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
      Thread.sleep(5000);
      driver.findElement(By.id("com.tatadhp.consumer.staging:id/tv_skip_done")).click();
      Thread.sleep(5000);
 System.out.println("Start");
 
		driver.findElement(By.id("com.tatadhp.consumer.staging:id/consumer_icon_title")).click();
 	System.out.println("ENd");
		
	}
	
}
