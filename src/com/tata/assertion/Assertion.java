package com.tata.assertion;


import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;






import com.tata.libraries.Resultsummary;
import com.tata.libraries.SetUp;

//*************************************************************************************************************************
	// Generic class name :Assertion
	// Description : Writing generic functions and validation methods
    //Author: Ganesh Jagatap
    
	// *************************************************************************************************************************

public class Assertion {
	
	Resultsummary rs = new Resultsummary();
	String scriptname = getClass().getSimpleName();

	SoftAssert sa = new SoftAssert();
	WebDriver drv;
	
	
	//*************************************************************************************************************************
		// Function name :assertString
		// Description : soft assert for comparing strings
	    //Author: Ganesh Jagatap
	    //Last Modified Date: 17-08-2016
		// *************************************************************************************************************************
	public void assertString(String actual, String expected, String msg) {
		sa.assertEquals(actual, expected, msg);
	}

	
	//*************************************************************************************************************************
			// Function name :assertAll
			// Description : Binding all assertions
		    //Author: Ganesh Jagatap
		    //Last Modified Date: 17-08-2016
			// *************************************************************************************************************************
	public void assertAll() {
		sa.assertAll();
	}

	//*************************************************************************************************************************
	// Function name :verifyTitle
	// Description : Comparing string (Hard assert)
    //Author: Ganesh Jagatap
    //Last Modified Date: 17-08-2016
	// *************************************************************************************************************************
	public void verifyTitle(String actual, String expected, String scriptname,String desc,String TCID)
			throws Exception {
		try {
			if (actual.equals(expected)) {

				rs.writePassLog(scriptname, desc,TCID);

			} else {
				rs.writeFailLog(scriptname, "not "+desc,TCID);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	//*************************************************************************************************************************
		// Function name :verifyText
		// Description : Comparing string using contains function
	    //Author: Ganesh Jagatap
	    //Last Modified Date: 17-08-2016
		// *************************************************************************************************************************

public void verifyText(String actual,String expected,String scriptname,String message,String TCID) throws Exception{
	
	try{
		
	
	if(actual.contains(expected)){
		rs.writePassLog(scriptname, message,TCID);
	}else{
		
		rs.writeFailLog(scriptname,"not"+message,TCID);
	}
	
}
	catch(Exception e){
		e.printStackTrace();
		rs.writeFailLog(scriptname,"not"+message,TCID);
	}

}


//*************************************************************************************************************************
		// Generic Function name :click
		// Description : Clicking on webelement
	    //Author: Ganesh Jagatap
	    //Last Modified Date: 17-08-2016
		// *************************************************************************************************************************

public void click(WebElement element,String scriptname,String desc){
	try{
		WebDriverWait wait = new WebDriverWait(SetUp.driver, 30);
		wait.until(ExpectedConditions.visibilityOf(element));
		 element.click();
		 rs.writePassLog(scriptname, desc,"");
	}catch(Exception e){
		
	}
	
}

//*************************************************************************************************************************
		// Function name :isDispalyed
		// Description : Verifying the webelement present or not
	    //Author: Ganesh Jagatap
	    //Last Modified Date: 17-08-2016
		// *************************************************************************************************************************

public void isDispalyed(WebElement element,String scriptname,String desc,String TCID) throws Exception{
	
	try{
		WebDriverWait wait = new WebDriverWait(SetUp.driver, 30);
		wait.until(ExpectedConditions.visibilityOf(element));
		if (element.isDisplayed()) {

			rs.writePassLog(scriptname, desc,TCID);

		} else {
			rs.writeFailLog(scriptname,
					"Failed to "+desc,TCID);

		}

	} catch (Exception e) {
		rs.writeFailLog(scriptname, "Failed to "+desc,"");
		
	}
}
	
	
//*************************************************************************************************************************
		// Generic Function name :isDispalyed
		// Description : Entering input in text box 
	    //Author: Ganesh Jagatap
	    //Last Modified Date: 17-08-2016
		// *************************************************************************************************************************	
	
	public void typeText(WebElement element,String scriptname,String desc,String enterText) throws Exception{
	try{
		WebDriverWait wait = new WebDriverWait(SetUp.driver, 60);
		wait.until(ExpectedConditions.visibilityOf(element));
		element.clear();
		element.sendKeys(enterText);
		rs.writePassLog(scriptname, desc,"");
		
	}catch(Exception e){
		rs.writeFailLog(scriptname, "Failed to "+desc,"");
	}
}

	//*************************************************************************************************************************
			// Generic Function name :getErrorText
			// Description :Returns validation message 
		    //Author: Ganesh Jagatap
		    //Last Modified Date: 17-08-2016
			// *************************************************************************************************************************	
			
public String  getErrorText(WebElement element){
	String retVal=null;
	try{
		WebDriverWait wait = new WebDriverWait(SetUp.driver, 20);
		wait.until(ExpectedConditions.visibilityOf(element));
		retVal=element.getText();
		
	}catch(Exception e){
		
	}
	
	return retVal;
}




//*************************************************************************************************************************
// Generic Function name :switchToFrame
// Description :Switching between frames 
//Author: Ganesh Jagatap
//Last Modified Date: 17-08-2016
//*************************************************************************************************************************	
public void switchToFrame(WebElement element){
	try{
	
		WebDriverWait wait = new WebDriverWait(SetUp.driver, 10);
		wait.until(ExpectedConditions.visibilityOf(element));	
	
		SetUp.driver.switchTo().frame(element);
		
		
	}catch(Exception e){
		
	}}

//*************************************************************************************************************************
//Generic Function name :switchToFrame
//Description :Returnig back to default frame 
//Author: Ganesh Jagatap
//Last Modified Date: 17-08-2016
//*************************************************************************************************************************	

public void switchToDefaultContent(){
	try{
	
			
		SetUp.driver.switchTo().defaultContent();
		
		
	}catch(Exception e){
		
	}
	}

//*************************************************************************************************************************
//Generic Function name :getTitle
//Description :Returnig title of the page
//Author: Ganesh Jagatap
//Last Modified Date: 17-08-2016
//*************************************************************************************************************************	


public String getTitle(){
	String title=null;
	try{
		title=SetUp.driver.getTitle();
	}
	catch(Exception e){
		
	}

	

return title;
}


//*************************************************************************************************************************
//Generic Function name :getTitle
//Description :Scrolling menu
//Author: Ganesh Jagatap
//Last Modified Date: 17-08-2016
//*************************************************************************************************************************	

public void scrollMenu(String scriptname,String desc) throws Exception{
	try{
		SetUp.driver.scrollTo("");
		rs.writePassLog(scriptname, desc,"");
		
	}catch(Exception e){
		rs.writeFailLog(scriptname, "Failed to "+desc,"");
	}
}

//*************************************************************************************************************************
//Generic Function name :switchToWindowToCheckElementDisplay
//Description :Switching between window and verifying the page
//Author: Ganesh Jagatap
//Last Modified Date: 17-08-2016
//*************************************************************************************************************************	
public void switchToWindowToCheckElementDisplay(WebElement element,String scriptname,String desc,String TCID) throws Exception{
	
	try {
		Set<String> list=SetUp.driver.getWindowHandles();
		Iterator<String> it=list.iterator();
		String parent=it.next();
		System.out.println("parent "+parent);
		String child=it.next();
		System.out.println("child "+child);
		SetUp.driver.switchTo().window(child);
		
		WebDriverWait wait = new WebDriverWait(SetUp.driver, 10);
		wait.until(ExpectedConditions.visibilityOf(element));
		if (element.isDisplayed()) {

			rs.writePassLog(scriptname,desc,TCID);

		} else {
			rs.writeFailLog(scriptname,
					desc,TCID);

		}
		SetUp.driver.close();
		SetUp.driver.switchTo().window(parent);
	} catch (Exception e) {
		rs.writeFailLog(scriptname, desc,TCID);
		
	}
	
	
}

//*************************************************************************************************************************
//Generic Function name :acceptAlert
//Description :clicking yes on alert pop up
//Author: Ganesh Jagatap
//Last Modified Date: 17-08-2016
//*************************************************************************************************************************	
public void acceptAlert(){
	
	try{
		
		Alert alert=SetUp.driver.switchTo().alert();
	
		alert.accept();
		
		
	}catch(Exception e){
		
	}
}

//*************************************************************************************************************************
//Generic Function name :MouseHover
//Description :Mouse hover on webelement
//Author: Ganesh Jagatap
//Last Modified Date: 17-08-2016
//*************************************************************************************************************************	

public void MouseHover(WebElement element, String scriptname,String desc,String TCID){
	
	try{
		WebDriverWait wait = new WebDriverWait(SetUp.driver, 10);
		wait.until(ExpectedConditions.visibilityOf(element));
		Actions act=new Actions(SetUp.driver);
		act.moveToElement(element);
		rs.writePassLog(scriptname, desc, TCID);
		
	}catch(Exception e){
		
	}
	
}

//*************************************************************************************************************************
//Generic Function name :switchToWindow
//Description :Switching between window 
//Author: Ganesh Jagatap
//Last Modified Date: 17-08-2016
//*************************************************************************************************************************	
public void switchToWindow(){
	
	try{
		Set<String> list=SetUp.driver.getWindowHandles();
		Iterator<String> it=list.iterator();
		String parent=it.next();
		System.out.println("parent "+parent);
		String child=it.next();
		System.out.println("child "+child);
		SetUp.driver.switchTo().window(child);
	}catch(Exception e){
		
	}
	
	
	
}

//*************************************************************************************************************************
//Generic Function name :Gmail_Validation
//Description :Validating email after transaction
//Author: Ganesh Jagatap
//Last Modified Date: 17-08-2016
//*************************************************************************************************************************	

public void selectNetbank(String scriptname) throws Exception
//android.widget.ListView
//android:id/select_dialog_listview
{
	
	
	
	//android:id/select_dialog_listview
	
//}




	
}

}