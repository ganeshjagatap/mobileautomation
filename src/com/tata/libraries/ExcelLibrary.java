package com.tata.libraries;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
//Fetching data from Excel sheet
public class ExcelLibrary {
   SetUp br = new SetUp();
	String path1 = br.testConfigFile.getProperty("excelPath");
	
	// ********************************************************************************************************************
		// Generic function name :getExcelData
		// Description : To fetch the value of the corresponding cell based on the
		// Excel, Sheet name, Row and Column specified
		// Author: Ganesh Jagatap
	 //Last Modified Date: 28-08-2016
// ********************************************************************************************************************
	
	
	public String getExcelData(String sheetName, int rowNum, int colNum) {
		String retVal = null;
		try {
			FileInputStream fis = new FileInputStream(path1);
			Workbook wb = WorkbookFactory.create(fis);
			Sheet s = wb.getSheet(sheetName);
			Row r = s.getRow(rowNum);
			Cell c = r.getCell(colNum);
			retVal = c.getStringCellValue();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return retVal;
	}

	// ********************************************************************************************************************
		// Generic function name :getRowCount
		// Description : To get the active row count of the Excel sheet.
		// Author: Ganesh Jagatap
	 //Last Modified Date: 28-08-2016
		// ********************************************************************************************************************
	
	
	public int getRowCount(String sheetName) {
		int retVal = 0;
		try {
			FileInputStream fis = new FileInputStream(path1);
			Workbook wb = WorkbookFactory.create(fis);
			Sheet s = wb.getSheet(sheetName);
			retVal = s.getLastRowNum();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return retVal;
	}
	

	// *************************************************************************************************************************
	// Generic function name :writeToExcel
	// Description : To write the content in to the corresponding cell based on
	// the Excel , Sheet name, Row and column specified.
	// Author: Ganesh Jagatap
	 //Last Modified Date: 28-08-2016
	// *************************************************************************************************************************

	public void writeToExcel(String sheetName, int rowNum, int colNum,
			String desc) {
		try {
			FileInputStream fis = new FileInputStream(path1);
			Workbook wb = WorkbookFactory.create(fis);
			Sheet s = wb.getSheet(sheetName);
			Row r = s.getRow(rowNum);
			Cell c = r.createCell(colNum);
			c.setCellValue(desc);
			FileOutputStream fos = new FileOutputStream(path1);
			wb.write(fos);
			fos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}