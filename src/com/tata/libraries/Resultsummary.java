package com.tata.libraries;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.tata.driver.MainDriver;

//creating result summary
public class Resultsummary {
	public static String pth = "";
	public static int failcounter;
	public static int slno = 1;
	public String Snapshotpath;
	public static int screencapcounter = 1;
	SetUp br = new SetUp();
	
	//********************************************************************************************************************
	//Generic function name :createSummaryFile
	//Description : To Create test automation summary file.
	//Author: Ganesh Jagatap
	 //Last Modified Date: 17--8-2016
	//********************************************************************************************************************
	public void createSummaryFile() throws IOException {
		try {

		
			String sfile = br.testConfigFile.getProperty("resultPath");
			System.out.println("sfile " + sfile);
			Calendar c = new GregorianCalendar();
			String currtime = c.getTime().toString();
			SetUp br = new SetUp();

			String Browser = br.testConfigFile.getProperty("browserType");
			String Url = br.testConfigFile.getProperty("Env");
			String Os = br.testConfigFile.getProperty("OS");
			String BrowserVersion = br.testConfigFile
					.getProperty("BrowserVersion");

			BufferedWriter bw1 = new BufferedWriter(new FileWriter(sfile
					+ "/Summary.html"));
			bw1.write("<html>");
			bw1.write("<head>");
			bw1.write("<title>Result Summary</title>");
			bw1.write("</head><body>");

			bw1.write("<table style=font-family:arial border =1 cellspacing=1 frame=Vsides bgcolor=#CC9999 Align=Center>");
			bw1.write("<tr><th width=775>TATA Mobile : Automated Test Script Execution Summary Report</th></tr>");
			bw1.write("</table>");

			bw1.write("<table style=font-family:calibri border =1 cellspacing=1 frame=Vsides bgcolor=6699FF Align=Center>");
			bw1.write("<tr><td  width=500 ><B>Execution Started Time</B></td><td width=270><B>"
					+ currtime + "</B></td></tr>");
			bw1.write("<tr><td  width=500 ><B>Environment </B></td><td width=270><B>"
					+ Url + "</B></td></tr>");
		/*	bw1.write("<tr><td  width=500 ><B>Browser </B></td><td width=270><B>"
					+ Browser + "</B></td></tr>");
			bw1.write("<tr><td  width=500 ><B>Browser Version </B></td><td width=270><B>"
					+ BrowserVersion + "</B></td></tr>");*/
			bw1.write("<tr><td  width=500 ><B>Operating System </B></td><td width=270><B>"
					+ Os + "</B></td></tr>");
			bw1.write("<tr><td  width=500 ><B>Automation Tool</B></td><td width=270><B>Appium</B></td></tr></table><br/>");
			bw1.write("<table style=font-family:calibri border =2 cellspacing=1 frame=Vsides bgcolor=#E8E8E8 Align=Center><tr><td Align=Center width=50><B>SL NO</B></td><td Align=center width=632><B>TestScript Name</B></td><td width=80 Align=center><B>Result</B></td><td width=80 Align=center><B>TestLink TC No.</B></td><td width=80 Align=center><B>Execution Time</B></td></tr>");
		//	bw1.flush();
			bw1.close();

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	//********************************************************************************************************************
	//Generic function name :writePassSummary
	//Description : Writing Pass Message to the html log file
	//Author: Ganesh Jagatap
	 //Last Modified Date: 17--8-2016
	//********************************************************************************************************************
	public void writePassSummary(String scriptname, String status,
			String TestLinkTCNo) throws IOException {
		try {

			String rpath = br.testConfigFile.getProperty("resultPath");
			System.out.println("rpath    " + rpath);
			BufferedWriter bw1 = new BufferedWriter(new FileWriter(rpath
					+ "/summary.html", true));
			bw1.write("<tr><td Align=center>" + slno + "</td><td><a href="
					+ scriptname + ".html>" + scriptname
					+ "</a></td><td bgcolor=#00FF00 Align=center>" + status
					+ "</td><td bgcolor=#00FF00 Align=center>" + TestLinkTCNo
					+ "</td><td bgcolor=#00FF00 Align=center>" + MainDriver.Exectime
					+ "</td></tr>");
			slno++;
			bw1.close();
		} catch (Exception e) {
			System.out.println("Write PassSummary error = " + e);
		}
	}

	//********************************************************************************************************************
	//Generic function name :writeFailSummary
	//Description : Writing Fail Message to the html log file
	//Author: Ganesh Jagatap
	 //Last Modified Date: 17--8-2016
	//********************************************************************************************************************
	public void writeFailSummary(String scriptname, String status,
			String TestLinkTCNo) throws IOException {
		try {

			String rpath = br.testConfigFile.getProperty("resultPath");
			BufferedWriter bw1 = new BufferedWriter(new FileWriter(rpath
					+ "/summary.html", true));
			bw1.write("<tr><td Align=center>" + slno + "</td><td><a href="
					+ scriptname + ".html>" + scriptname
					+ "</a></td><td bgcolor=#FF0000 Align=center>" + status
					+ "</td><td bgcolor=#00FF00 Align=center>" + TestLinkTCNo
					+  "</td><td bgcolor=#00FF00 Align=center>" + MainDriver.Exectime
					+ "</td></tr>");
			slno++;

			bw1.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	
	//********************************************************************************************************************
	//Generic function name :createLogFile
	//Description : Creating Html Log File for the test script
	//Author: Ganesh Jagatap
	 //Last Modified Date: 17--8-2016
	//********************************************************************************************************************
	
	
	public void createLogFile(String scriptname) throws IOException {
		try {

			String rpath = br.testConfigFile.getProperty("resultPath");
			Calendar c = new GregorianCalendar();
			String currtime = c.getTime().toString();
			BufferedWriter bw1 = new BufferedWriter(new FileWriter(rpath + "/"
					+ scriptname + ".html"));
			bw1.write("<html><head><title>"
					+ scriptname
					+ "</title></head><body><table style=font-family:calibri border =1 cellspacing=1 frame=Vsides  bgcolor=6699FF Align=Center><tr><td><B>Test Case Name</B></td><td><B>"
					+ scriptname + "</B></td></tr>");
			bw1.write("<tr><td width=500 ><B>Execution Start Time</B></td><td width=270><B>"
					+ currtime + "</B></td></tr></table><br/>");
			bw1.write("<table style=font-family:calibri border =1 cellspacing=1 frame=Vsides bgcolor=#E8E8E8 Align=Center><tr><td Align=center><B>Step Description</B></td><td Align=center><B>Testcase ID</B></td><td Align=center><B>Result</B></td></tr>");
			bw1.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	//********************************************************************************************************************
	//Generic function name :writePassLog
	//Description : Writing Pass message to the HTML log file
	//Author: Ganesh Jagatap
	 //Last Modified Date: 17--8-2016
	//********************************************************************************************************************
	public void writePassLog(String scriptname, String desc,String TCID) throws IOException {
		try {

			String rpath = br.testConfigFile.getProperty("resultPath");
			BufferedWriter bw1 = new BufferedWriter(new FileWriter(rpath + "/"
					+ scriptname + ".html", true));
			bw1.write("<tr><td width=690>"
					+ desc
					+ "</td><td width=80 bgcolor=#00FFD5 Align=center>"
					+ TCID
					+ "</td><td width=80 bgcolor=#00FF00 Align=center >PASS</td></tr>");
			bw1.close();
		} catch (Exception e) {
			System.out.println("Error = " + e);
		}
	}

	//********************************************************************************************************************
	//Generic function name :writeFailLog
	//Description : Writing Fail message to the HTML log file
	//Author: Ganesh Jagatap
	 //Last Modified Date: 17--8-2016
	//********************************************************************************************************************
	public void writeFailLog(String scriptname, String desc,String TCID) throws Exception {
		try {

			String rpath = br.testConfigFile.getProperty("resultPath");
			System.out.println("rpath "+rpath);
			Snapshotpath = rpath;
			
			Capturescreenshot(scriptname);
			BufferedWriter bw1 = new BufferedWriter(new FileWriter(rpath + "/"
					+ scriptname + ".html", true));
			bw1.write("<tr><td width=690><a href="
					+Snapshotpath+">"+desc+"</a>+</td><td width=80 bgcolor=#00FFD5 Align=center>"
					+ TCID+"</td><td width=80 bgcolor=#FF0000 Align=center >FAIL</td></tr>");
			/*
			 * bw1.write("<tr><td width=690><a href=" + "" + ">" + desc +
			 * "</a></td><td width=80 bgcolor=#FF0000 Align=center >FAIL</td></tr>"
			 * );
			 */
			bw1.close();
			failcounter++;
		} catch (Exception e) {
			System.out.println("Write Failed Log Exception = " + e);
		}
	}

	//********************************************************************************************************************
	//Generic function name :Capturescreenshot
	//Description : Capture the screen shot and store in the Result folder
	//Author: Ganesh Jagatap
	 //Last Modified Date: 17--8-2016
	//********************************************************************************************************************
	
	public void Capturescreenshot(String scriptname) throws Exception {
		String rpath=null;
		 rpath = br.testConfigFile.getProperty("resultPath");
		try {
			File srcFile = ((TakesScreenshot) SetUp.driver)
					.getScreenshotAs(OutputType.FILE);
			// Once we have the screenshot in our file 'srcFile' you can use all
			// FileUtils methods like
			Snapshotpath = rpath + scriptname + screencapcounter + ".png";
		
			System.out.println("snapshot path " + Snapshotpath);
			srcFile.renameTo(new File(Snapshotpath));
		} catch (Exception e) {
			e.printStackTrace();
		}
		screencapcounter++;

	}

}