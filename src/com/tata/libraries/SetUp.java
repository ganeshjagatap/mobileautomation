package com.tata.libraries;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class SetUp {
	// controlling browser and url
	 public static AppiumDriver driver;
	private FileInputStream configFile = null;
	public Properties testConfigFile = null;
	String chromedrvpath = null;
	String iedrvpath = null;

	//*************************************************************************************************************************
		//Generic function name :SetUp
		//Description :Setting up the desired capabilities
		//Author: Ganesh Jagatap
		//Last Modified Date: 28-08-2016
		//*************************************************************************************************************************	

	public SetUp() {
		try {
			configFile = new FileInputStream("./config.properties");
			testConfigFile = new Properties();
			testConfigFile.load(configFile);
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	 
	public void open() throws MalformedURLException {
		String ScrRunInterface = testConfigFile.getProperty("ScrRunInterface");
		String apk = testConfigFile.getProperty("apk");
		System.out.println("apk pathhh "+apk);
		File app = new File(apk);
		DesiredCapabilities capabilities = new DesiredCapabilities();
	try{	
		
		
		if(ScrRunInterface.equalsIgnoreCase("appium")){
		 
    	
		capabilities.setCapability(CapabilityType.BROWSER_NAME, "");
		 capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "4.4");
		 capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME,"appium"); 
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Samsung Galaxy S5 Emulator"); 
		capabilities.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
		
       capabilities.setCapability("app-package", "com.tatadhp.consumer.staging");  
      capabilities.setCapability("app-activity", "com.tatadhp.consumer.ui.activities.WalkThroughActivity");  
       capabilities.setCapability("app-wait-activity","com.tatadhp.consumer.ui.activities.WalkThroughActivity");
		capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 80);

       driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
       
       
		}else if(ScrRunInterface.equalsIgnoreCase("selendroid")){
			capabilities.setCapability(CapabilityType.BROWSER_NAME, "");
			 capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "4.1");
			 capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME,"Selendroid"); 
			capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Emulator"); 
			capabilities.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
			
	        capabilities.setCapability("app-package", "com.tatadhp.consumer.staging");  
	        capabilities.setCapability("app-activity", "com.tatadhp.consumer.ui.activities.WalkThroughActivity"); 
	        capabilities.setCapability("app-wait-activity","com.tatadhp.consumer.ui.activities.WalkThroughActivity");
			capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 80);

	       driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
       
       
       
		}
       
	}catch(Exception e){
		
	}
	
	
	
	
	
	
	     
}
	

	// choosing url
	public void ConnectToUrl() {

		String testUrl = testConfigFile.getProperty("testurl");
		driver.get(testUrl);
	}


	public void close() {
		driver.quit();

	}

	// global wait
	public void globalWait(long time) {
		driver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);
	}

	public void globalWait() {
		String str = testConfigFile.getProperty("globaltime");
		long time = Long.parseLong(str);
		driver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);
	}

	// Run the windows based scenarios using AutoIt
	public void RunAutoit() {
		try {
			Runtime.getRuntime().exec(System.getProperty(""));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}