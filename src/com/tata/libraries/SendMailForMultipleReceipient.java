package com.tata.libraries;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

//*************************************************************************************************************************
//Generic Class name :SendMailForMultipleReceipient
//Description :Sending execution report for multiple recipients
//Author: Ganesh Jagatap
//Last Modified Date: 17-08-2016
//*************************************************************************************************************************	
public class SendMailForMultipleReceipient {
SetUp br=new SetUp();
	 public static String[] Esplit;
	  public static String html_Text;
	  Properties emailProperties;
	  Session mailSession;
	  MimeMessage emailMessage;
	  String Url = br.testConfigFile.getProperty("Env");
	  
	  
	//*************************************************************************************************************************
			//Function name :sendMail
			//Description :sending mail as per recipients
			//Author: Ganesh Jagatap
			//Last Modified Date: 17-08-2016
			//*************************************************************************************************************************	
	  public void sendMail(String userName, String toaddress[], String msg) throws AddressException, MessagingException, IOException
	  {  
	 
		  SendMailForMultipleReceipient javaEmail = new SendMailForMultipleReceipient();
		 
		 
	    javaEmail.setMailServerProperties();
	    javaEmail.createEmailMessage();
	    javaEmail.sendEmail();
	  }
	  
	 
	//*************************************************************************************************************************
	//Function name :setMailServerProperties
	//Description :Setting up email port and authentication
	//Author: Ganesh Jagatap
	//Last Modified Date: 17-08-2016
	//*************************************************************************************************************************	
	  public void setMailServerProperties() {
	 
	    String emailPort = "587";//gmail's smtp port//587
	 
	    emailProperties = System.getProperties();
	    emailProperties.put("mail.smtp.port", emailPort);
	    emailProperties.put("mail.smtp.auth", "true");
	    emailProperties.put("mail.smtp.starttls.enable", "true");
	 
	  }
	 
	  
	//*************************************************************************************************************************
		//Function name :createEmailMessage
		//Description :Adding subject and message body
		//Author: Ganesh Jagatap
		//Last Modified Date: 17-08-2016
		//*************************************************************************************************************************	
	  public void createEmailMessage() throws AddressException,
	      MessagingException, IOException {
	    
			String Url = br.testConfigFile.getProperty("Env");
	    String emailSubject = "TATA Mobile Automation Execution Result --"+(Url)+"";
	    
	    InetAddress ownIP=InetAddress.getLocalHost();
		System.out.println("IP of my system is := "+ownIP.getHostAddress());
		String path = br.testConfigFile.getProperty("toaddress");
	    Esplit = path.split(",");
		
		
	    Calendar currentDate = Calendar.getInstance();
		  SimpleDateFormat formatter= 
		  new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		  String dateNow = formatter.format(currentDate.getTime());
		  System.out.println("Now the date is :=>  " + dateNow);
	    
	   URL url = new URL("file:///E:/tata_Backup/Ganesh/backup/TataMobileAutomation/results/Summary.html");
		URLConnection conn = url.openConnection();
		DataInputStream in = new DataInputStream ( conn.getInputStream (  )  ) ;
		BufferedReader d = new BufferedReader(new InputStreamReader(in));
		String text = d.readLine();		
		
		int html = text.indexOf("Appium");		
		int index = html+20;
		String text3 = text.substring(index);
		html_Text = text3.replace("href", "");

		String rpath = br.testConfigFile.getProperty("resultPath");
		System.out.println("rpath    " + rpath);
		BufferedWriter bw1 = new BufferedWriter(new FileWriter(rpath
				+ "/summary.html", true));
		
			
		  
		  
		  
		String emailBody = "</tr>"
			  	+ "<html><body>"
				+"<p><h1>TATA Mobile Automation Execution Result Report --"+(Url)+"</h1></p>"
				/*+"<p><h3>Date of Execution : "+dateNow+"</h3></p>"
				+ "<p><h3>Please find Complete Execution Result @ "+"<a href="+Result_Pth+">Deal Chicken</a>"+"\\Results*"
				+ "<p><h4>*Note: To access the detailed reports in Dropbox account, login with 'dealchicken@gmail.com/dcautomation' credentials</h3><p>"*/
				+ html_Text
				+"</table>";
			

				
		
	    mailSession = Session.getDefaultInstance(emailProperties, null);
	    emailMessage = new MimeMessage(mailSession);
	  
	 
	    for (int i = 0; i < Esplit.length; i++) {
	      emailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(Esplit[i]));
	    }
	    String x=emailMessage.getContentType();
	    System.out.println("Content type "+x);
	    
	    emailMessage.setSubject(emailSubject);
	 //   emailMessage.setFlag(flag, set);
	    emailMessage.setContent(emailBody, "text/html");
	    //for a html email
	  //  emailMessage.setContent(emailBody, "application/x-javascript");
	    //emailMessage.setContent(emailBody, "text/javascript");
	    String x1=emailMessage.getContentType();
	    System.out.println("Content type "+x1);
	    //emailMessage.setText(emailBody);// for a text email
	 
	  }
	 
	  
	  
	  
	  public void sendEmail() throws AddressException, MessagingException {
	 
	    String emailHost = "smtp.gmail.com";
	    String fromUser = "ganesh.jagatap";//just the id alone without @gmail.com
	    String fromUserEmailPassword = "tata@1234";
	 
	    Transport transport = mailSession.getTransport("smtp");
	 
	    transport.connect(emailHost, fromUser, fromUserEmailPassword);
	    transport.sendMessage(emailMessage, emailMessage.getAllRecipients());
	    transport.close();
	    System.out.println("Email sent successfully.");
	  }
	
	
	  public String[] splitdatabycoma(String data)

	  {

	   String[] splits = data.split(",");

	  return splits;

	  }

	
	
	
}
