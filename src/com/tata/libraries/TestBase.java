package com.tata.libraries;

import java.io.IOException;

import org.apache.commons.exec.ExecuteException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

//*************************************************************************************************************************
//Generic Class name :Testbase
//Description :TestsNG methods exection before and after each scripts
//Author: Ganesh Jagatap
//Last Modified Date: 17-08-2016
//*************************************************************************************************************************	


public class TestBase extends SetUp {

	//*************************************************************************************************************************
	//Generic function  name :Start
	//Description :configuring appium set up
	//Author: Ganesh Jagatap
	//Last Modified Date: 17-08-2016
	//*************************************************************************************************************************	

	@BeforeMethod
	
	public void Start() throws InterruptedException, ExecuteException, IOException{
		
		open();
		Thread.sleep(8000);

		
	}
	//*************************************************************************************************************************
		//Generic function name :quit
		//Description :quiting the session
		//Author: Ganesh Jagatap
		//Last Modified Date: 17-08-2016
		//*************************************************************************************************************************	

	@AfterMethod
	public void quit(){
		//close();
		
		
	}
}